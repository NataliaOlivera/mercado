package mercado.mercado;

import org.junit.Test;

import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.time.YearMonth;
import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.*;

import mercado.clientes.*;
import mercado.facturacion.*;
import mercado.excepciones.*;


public class LibroFacturasTest {
    ResponsableMercado responsable;
    Mercado mercado;
    LibroFacturas libroFacturas;
    Puesto puesto;
    Medidor medidor;
    Sector sector;
    Cliente cliente,cliente2;
    Contrato contrato;
    LecturaMedidor lecturaMedidor;
    @Before
    public void initObjects(){
      mercado = new Mercado("Abasto","Av.Ocampo",4700,38345358,4);
      responsable = new ResponsableMercado("Evelyn", "Pereyra","16342845",mercado);
      libroFacturas =new LibroFacturas();
      sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
      medidor = new Medidor(45);
      puesto = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor,sector);
      sector.agregarPuesto(puesto);
      cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
      contrato = new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto,cliente,responsable);
      lecturaMedidor = new LecturaMedidor(YearMonth.parse("2020-12"),new BigDecimal(234) ,new BigDecimal(13),83);
      cliente2 = new Quintero(2,"Matias Ganne","27457284300", "domicilio", "0301246000", "mati@gmail.com");
    }

    @Test 
    public void agregarFacturasyEncontrarFactura(){
        Factura factura1 = new Factura(1,LocalDate.parse("2020-10-10"), responsable, cliente,"Descripcion");
        Factura factura2 = new Factura(2,LocalDate.parse("2019-03-04"), responsable, cliente,"Descripcion");
        libroFacturas.agregarFactura(factura1);
        libroFacturas.agregarFactura(factura2);
        Factura facturaEncontrada =libroFacturas.encontrarFactura(2);
        assertEquals(factura2,facturaEncontrada);
    }
    
    @Test 
    public void eliminarFacturaTest(){
        Factura factura1 = new Factura(1,LocalDate.parse("2020-10-10"), responsable, cliente,"Descripcion");
        Factura factura2 = new Factura(2,LocalDate.parse("2019-03-04"), responsable, cliente,"Descripcion");
        libroFacturas.agregarFactura(factura1);
        libroFacturas.agregarFactura(factura2);
        libroFacturas.eliminarFactura(2);
        assertEquals((Integer) 1, libroFacturas.getCantidadFacturas());
    }

    @Test 
    public void modificarFacturaTest(){
        Factura facturaAModificar = new Factura(1,LocalDate.parse("2020-10-10"), responsable, cliente,"Descripcion");
        Factura facturaNueva = new Factura(2,LocalDate.parse("2019-03-04"), responsable, cliente,"Descripcion");
        libroFacturas.agregarFactura(facturaAModificar);
        libroFacturas.modificarFactura(facturaAModificar,facturaNueva);
        assertEquals(facturaNueva,libroFacturas.encontrarFactura(2));
    }
    
    @Test 
    public void encontrarFacturasDeUnMesTest(){
        Factura factura1 = new Factura(1,LocalDate.parse("2020-10-10"), responsable, cliente,"Descripcion");
        Factura factura2 = new Factura(2,LocalDate.parse("2019-09-04"), responsable, cliente,"Descripcion");
        Factura factura3 = new Factura(3,LocalDate.parse("2020-09-15"), responsable, cliente,"Descripcion");
        Factura factura4 = new Factura(4,LocalDate.parse("2020-09-10"), responsable, cliente2,"Descripcion");
        Factura factura5 = new Factura(5,LocalDate.parse("2020-12-10"), responsable, cliente,"Descripcion");
        libroFacturas.agregarFactura(factura1);
        libroFacturas.agregarFactura(factura2);
        libroFacturas.agregarFactura(factura3);
        libroFacturas.agregarFactura(factura4);
        libroFacturas.agregarFactura(factura5);
        ArrayList<Factura> facturasDeSeptiembre = libroFacturas.buscarFacturasDeUnMes(9);
        assertEquals(3,facturasDeSeptiembre.size());
    }

    @Test 
    public void encontrarFacturasEnUnPeriodoTest(){
        Factura factura1 = new Factura(1,LocalDate.parse("2020-10-10"), responsable, cliente,"Descripcion");
        Factura factura2 = new Factura(2,LocalDate.parse("2019-09-04"), responsable, cliente,"Descripcion");
        Factura factura3 = new Factura(3,LocalDate.parse("2020-09-15"), responsable, cliente,"Descripcion");
        Factura factura4 = new Factura(4,LocalDate.parse("2020-09-10"), responsable, cliente2,"Descripcion");
        Factura factura5 = new Factura(5,LocalDate.parse("2020-12-10"), responsable, cliente,"Descripcion");
        libroFacturas.agregarFactura(factura1);
        libroFacturas.agregarFactura(factura2);
        libroFacturas.agregarFactura(factura3);
        libroFacturas.agregarFactura(factura4);
        libroFacturas.agregarFactura(factura5);
        ArrayList<Factura> facturasEncontradas = libroFacturas.encontrarFacturasEnUnPeriodo(LocalDate.parse("2019-08-07"), LocalDate.parse("2020-09-30"));
        assertEquals(3,facturasEncontradas.size());
    }



    @Test (expected = FacturaInexistenteException.class)
    public void buscarUnaFacturaInexistente(){
      libroFacturas.encontrarFactura(123);
    }

    @Test (expected = FacturaExistenteException.class)
    public void agregarUnaFacturaYaExistente(){
      Factura factura = new Factura(1,LocalDate.parse("2020-10-10"), responsable, cliente,"Descripcion");
      libroFacturas.agregarFactura(factura);
      libroFacturas.agregarFactura(factura);
    }

    @Test (expected = FacturasNoEncontradasException.class)
    public void buscarGrupoDeFacturasInexistentes(){
      libroFacturas.buscarFacturasDeUnMes(11);
    }
    @Test (expected = FacturasNoEncontradasException.class)
    public void buscarGrupoDeFacturasDePeriodoInexistente(){
      libroFacturas.encontrarFacturasEnUnPeriodo(LocalDate.parse("2020-10-10"),LocalDate.parse("2020-10-10"));
    }
    
    
  @Test 
    public void calcularImporteDeFacturasDeUnMesTest(){
        Factura factura1 = new Factura(1,LocalDate.parse("2020-10-01"),responsable, cliente,"Descripcion");
        Factura factura2 = new Factura(2,LocalDate.parse("2019-09-04"), responsable, cliente,"Descripcion");
        Factura factura3 = new Factura(3,LocalDate.parse("2020-09-15"), responsable, cliente,"Descripcion");
        Factura factura4 = new Factura(4,LocalDate.parse("2020-09-10"), responsable, cliente,"Descripcion");
        libroFacturas.agregarFactura(factura1);
        libroFacturas.agregarFactura(factura2);
        libroFacturas.agregarFactura(factura3);
        libroFacturas.agregarFactura(factura4);
        factura1.agregarConceptos(contrato);
        factura1.agregarConceptos(lecturaMedidor);   
        factura1.getTotalAPagar();
        factura2.agregarConceptos(contrato);
        factura2.agregarConceptos(lecturaMedidor);   
        factura2.getTotalAPagar();
        factura3.agregarConceptos(contrato);
        factura3.agregarConceptos(lecturaMedidor);   
        factura3.getTotalAPagar();
        factura4.agregarConceptos(contrato);
        factura4.agregarConceptos(lecturaMedidor);   
        factura4.getTotalAPagar();
        BigDecimal importeTotal = libroFacturas.calcularImporteDeFacturasDeUnMes(9);
        assertEquals(new BigDecimal(51876),importeTotal);   
    }
}
