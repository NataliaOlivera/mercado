package mercado.mercado;

import org.junit.Test;

import mercado.excepciones.*;

public class ResponsableMercadoTest {

    ////////////////////EXCEPCIONES///////////////
    @Test (expected = CaracteresNoValidosEnNombreException.class)
    public void crearResponsableMercadoConNombreNoValida(){
        Mercado mercado = new Mercado("Abasto","Av.Ocampo",4700,38345358,4);
        ResponsableMercado responsable = new ResponsableMercado("Evelyn33", "Pereyra", "Eve",mercado);
    }

    @Test (expected = CaracteresNoValidosEnApellidoException.class)
    public void crearResponsableMercadoConApellidoNoValida(){
        Mercado mercado = new Mercado("Abasto","Av.Ocampo",4700,38345358,4);
        ResponsableMercado responsable = new ResponsableMercado("Evelyn", "Pereyra345", "Eve",mercado);
    }

    @Test (expected = CaracteresNoValidosEnDNIException.class)
    public void crearResponsableMercadoConDNINoValida(){
        Mercado mercado = new Mercado("Abasto","Av.Ocampo",4700,38345358,4);
        ResponsableMercado responsable = new ResponsableMercado("Evelyn", "Pereyra"," 123seger",mercado);
    }


}
