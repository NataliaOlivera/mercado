package mercado.mercado;

import org.junit.*;

import mercado.excepciones.*;

public class UsuarioTest {
    ////////////////////EXCEPCIONES///////////////

    @Test (expected = ErrorAlIngresarDNIException.class)
    public void controlarElIngresoDelDniDelEmpleado(){
        Usuario usuario1= new Usuario("Olivera","Natalia","4059ff","natalia","abc123");
    }

    @Test (expected = ErrorAlIngresarApellidoDelEmpleadoException.class)
    public void controlarElIngresoDelApellidoDelEmpleado(){
        Usuario usuario1= new Usuario("Olivera 22","Natalia","40598438","natalia","abc123");
    }

    @Test (expected = ErrorAlIngresarNombreDelEmpleadoException.class)
    public void controlarElIngresoDelNombreDelEmpleado(){
        Usuario usuario1= new Usuario("Olivera","Natalia   2221","40598438","natalia","abc123");
    }
}
