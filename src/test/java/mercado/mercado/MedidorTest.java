package mercado.mercado;

import org.junit.Test;
import java.time.YearMonth;
import java.math.BigDecimal;
import org.junit.*;
import static org.junit.Assert.assertEquals;

import mercado.excepciones.*;

public class MedidorTest {

  ResponsableMercado responsable;
  Mercado mercado;
  Sector sector;
  Medidor medidor;

  @Before
  public void initObjects(){
      mercado = new Mercado("Abasto","Av.Ocampo",4700,38345358,3);
      responsable = new ResponsableMercado("Evelyn", "Pereyra","16342845",mercado);
      sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
      medidor = new Medidor(45);
  }

  @Test 
    public void crearMedidorEnPuesto(){
        Puesto puesto = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor,sector); 
        assertEquals(medidor,puesto.getMedidor());
    }    

    @Test 
    public void agregarLecturasYBuscarUnaLectura(){
      LecturaMedidor lectura1 = new LecturaMedidor(YearMonth.parse("2020-03"), new BigDecimal(300), new BigDecimal(2.5),83);
      LecturaMedidor lectura2 = new LecturaMedidor(YearMonth.parse("2020-10"), new BigDecimal(300), new BigDecimal(2.5),34);
      LecturaMedidor lectura3 = new LecturaMedidor(YearMonth.parse("2019-09"), new BigDecimal(300), new BigDecimal(2.5),25);
      medidor.agregarLectura(lectura1);
      medidor.agregarLectura(lectura2);
      medidor.agregarLectura(lectura3);
      LecturaMedidor lecturaEncontrada = medidor.buscarLectura(YearMonth.parse("2020-11"));
      assertEquals(lectura2,lecturaEncontrada);
      
    } 
    
    @Test 
    public void eliminarLectura(){
      LecturaMedidor lectura1 = new LecturaMedidor(YearMonth.parse("2020-03"), new BigDecimal(300), new BigDecimal(2.5),83);
      LecturaMedidor lectura2 = new LecturaMedidor(YearMonth.parse("2020-10"), new BigDecimal(300), new BigDecimal(2.5),34);
      LecturaMedidor lectura3 = new LecturaMedidor(YearMonth.parse("2019-09"), new BigDecimal(300), new BigDecimal(2.5),25);
      medidor.agregarLectura(lectura1);
      medidor.agregarLectura(lectura2);
      medidor.agregarLectura(lectura3);
      medidor.eliminarLectura(YearMonth.parse("2020-11"));
      assertEquals((Integer) 2,(Integer) medidor.getCantidadLecturas());
      
    } 

    @Test 
    public void modificarLectura(){
      LecturaMedidor lecturaAModificar = new LecturaMedidor(YearMonth.parse("2020-03"), new BigDecimal(300), new BigDecimal(2.5),83);
      LecturaMedidor lecturaNueva = new LecturaMedidor(YearMonth.parse("2020-03"), new BigDecimal(500), new BigDecimal(13),25);
      medidor.agregarLectura(lecturaAModificar);
      medidor.modificarLectura(lecturaAModificar, lecturaNueva);
      assertEquals(lecturaNueva,medidor.buscarLectura(YearMonth.parse("2020-04")));
    } 

    @Test (expected = LecturaInexistenteException.class)
      public void buscarUnaLecturaInexistente(){
        medidor.buscarLectura(YearMonth.parse("2020-09"));
      }

      @Test (expected = LecturaExistenteException.class)
      public void agregarUnaLecturaYaExistente(){
        LecturaMedidor lectura1 = new LecturaMedidor(YearMonth.parse("2020-03"), new BigDecimal(300), new BigDecimal(2.5),83);
        medidor.agregarLectura(lectura1);
        medidor.agregarLectura(lectura1);
      }



}

