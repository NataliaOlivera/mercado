package mercado.mercado;

import org.junit.Test;
import java.time.YearMonth;
import java.math.BigDecimal;
import static org.junit.Assert.*;


public class LecturaMedidorTest {
    
  @Test 
    public void calcularImporteDeConsumoMensual(){
        LecturaMedidor lecturaMedidor = new LecturaMedidor(YearMonth.parse("2020-12"),new BigDecimal(234) ,new BigDecimal(13),83);
        BigDecimal importeMensual = lecturaMedidor.getImporteMensual();
        assertEquals(new BigDecimal (3042),importeMensual);
    }


}
