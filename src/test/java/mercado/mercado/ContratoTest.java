package mercado.mercado;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.*;

import mercado.clientes.*;
import mercado.excepciones.*;

public class ContratoTest {
    ResponsableMercado responsable;
    Mercado mercado;
    @Before
    public void initObjects(){
        mercado = new Mercado("Abasto","Av.Ocampo",4700,38345358,4);
        responsable = new ResponsableMercado("Evelyn", "Pereyra","16342845",mercado);
    }

    @Test 
    public void verificarMontoMensual(){
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new Medidor(45);
        Puesto puesto = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor,sector);
        sector.agregarPuesto(puesto);
        Cliente persona= new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Contrato contrato=new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"), puesto, persona,responsable);
        BigDecimal importeMensualEsperado=contrato.getImporteMensual();
        assertEquals(new BigDecimal(14250),importeMensualEsperado);

    }


    @Test
    public void verificarCalculoPrecioAlquiler(){
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new Medidor(45);
        Puesto puesto = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor,sector);
        sector.agregarPuesto(puesto);
        Cliente persona= new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Contrato contrato=new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"), puesto, persona,responsable);
        BigDecimal precioAlquilerEsperado=contrato.getPrecioAlquiler();
        assertEquals(new BigDecimal(28500),precioAlquilerEsperado);
    }

    @Test
    public void calcularCantidadDeMesesDelContrato(){
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new Medidor(45);
        Puesto puesto = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor,sector);
        sector.agregarPuesto(puesto);
        Cliente persona= new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Contrato contrato=new Contrato(1,LocalDate.parse("2020-05-01"),LocalDate.parse("2020-12-24"), puesto, persona,responsable);
        int mesesDeDuracion = contrato.getCantidadMesesDeDuracion();
        assertEquals((Integer)7,(Integer)mesesDeDuracion);
    }


    @Test (expected = ContratoExistenteException.class)
    public void verificarQueNoSeAgregueElMismoContratoMasDeUnaVez(){
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new Medidor(45);
        Puesto puesto = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor,sector);
        sector.agregarPuesto(puesto);
        Cliente persona= new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Contrato contrato=new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"), puesto, persona,responsable);
        mercado.agregarContratos(contrato);
        mercado.agregarContratos(contrato);
    }


}