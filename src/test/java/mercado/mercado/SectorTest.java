package mercado.mercado;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;

import mercado.excepciones.*;

public class SectorTest {
  

    @Test public void agregarPuesto() {
        Sector sector = new Sector(156,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor1 = new Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor1, sector);
        Medidor medidor2 = new Medidor(50);
        Puesto puesto2 = new Puesto(456,true,new BigDecimal(10),true,true,"comentario",medidor2, sector);
        sector.agregarPuesto(puesto1);
        sector.agregarPuesto(puesto2);
        assertEquals(puesto2,sector.encontrarPuesto(456));
    }

    @Test public void encontrarPuesto() {
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor1 = new Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor1, sector);
        Medidor medidor2 = new Medidor(50);
        Puesto puesto2 = new Puesto(345,true,new BigDecimal(10),true,true,"comentario",medidor2, sector);
        Medidor medidor3 = new Medidor(60);
        Puesto puesto3 = new Puesto(134,true,new BigDecimal(15),true,true,"comentario",medidor3, sector);
        Medidor medidor4 = new Medidor(70);
        Puesto puesto4 = new Puesto(567,true,new BigDecimal(20),true,true,"comentario",medidor4, sector);
        sector.agregarPuesto(puesto1);
        sector.agregarPuesto(puesto2);
        sector.agregarPuesto(puesto3);
        sector.agregarPuesto(puesto4);
        Puesto puestoEncontrado = sector.encontrarPuesto(134);//Encontrar puesto con el numero 134
        assertEquals((Integer) 134,(Integer) puestoEncontrado.getNumeroPuesto());
    }

   @Test public void habilitarPuesto() {
        Sector sector=new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor1=new  Medidor(45);
        Puesto puesto1=new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor1, sector);
        Medidor medidor2=new  Medidor(50);
        Puesto puesto2=new Puesto(345,false,new BigDecimal(10),true,true,"comentario",medidor2, sector);
        sector.agregarPuesto(puesto1);
        sector.agregarPuesto(puesto2);
        sector.habilitarPuesto(345);
        assertEquals(true,puesto2.getDisponible());
    }
    @Test public void deshabilitarPuesto() {
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor1 = new  Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor1,sector);
        Medidor medidor2 = new  Medidor(50);
        Puesto puesto2 = new Puesto(345,true,new BigDecimal(10),true,true,"comentario",medidor2, sector);
        sector.agregarPuesto(puesto1);
        sector.agregarPuesto(puesto2);
        sector.deshabilitarPuesto(345);
        assertEquals(false,puesto2.getDisponible());
    }

    @Test public void eliminarUnPuesto() {
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor1 = new  Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor1, sector);
        Medidor medidor2 = new  Medidor(50);
        Puesto puesto2 = new Puesto(345,true,new BigDecimal(10),true,true,"comentario",medidor2 ,sector);
        sector.agregarPuesto(puesto1);
        sector.agregarPuesto(puesto2);
        sector.eliminarUnPuesto(345);
        assertEquals((Integer)1,(Integer) sector.getCantidadPuestos());
    }

    @Test (expected = PuestoExistenteException.class)
        public void agregarPuestoExistente(){
            Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
            Medidor medidor = new  Medidor(45);
            Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor,sector);    
            sector.agregarPuesto(puesto1);
            sector.agregarPuesto(puesto1);
    }

    @Test (expected = PuestoInexistenteException.class)
        public void eliminarPuestoInexistente(){
            Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
            sector.eliminarUnPuesto(234);
    }
    @Test (expected = PuestoInexistenteException.class)
        public void habilitarPuestoInexistente(){
            Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
            sector.habilitarPuesto(245);
    }
    

    

    @Test (expected = PuestoEnContratoException.class)
    public void eliminarPuestoEnContratoVigente(){
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new  Medidor(45);
        Puesto puesto = new Puesto(123,false,new BigDecimal(15),true,true,"comentario",medidor, sector);
        sector.agregarPuesto(puesto);
        sector.eliminarUnPuesto(123);
}


}

