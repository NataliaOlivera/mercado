package mercado.mercado;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;

public class PuestoTest {

    @Test public void comprobarNuevoPuesto() {
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new  Medidor(45);
        Puesto puesto = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        assertEquals((Integer) 123,(Integer) puesto.getNumeroPuesto());
    }

    @Test
    public void verificarElPrecioDelPuesto(){
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new  Medidor(45);
        Puesto puesto = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        sector.agregarPuesto(puesto);
        BigDecimal precioPuestoEsperado=puesto.getPrecioPuesto();
        assertEquals(new BigDecimal(14250),precioPuestoEsperado);

    }

   
}

