package mercado.mercado;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.*;
import static org.junit.Assert.*;

import mercado.clientes.*;
import mercado.excepciones.*;

public class MercadoTest {
    ResponsableMercado responsable;
    Mercado mercado;
    @Before
    public void initObjects(){
        mercado = new Mercado("Abasto","Av.Ocampo",4700,38345358,3);
        responsable = new ResponsableMercado("Evelyn", "Pereyra","16342845",mercado);
    }


    @Test public void modificarResponsable(){
        ResponsableMercado responsableNuevo = new ResponsableMercado("Natalia", "Olivera","30973843",mercado);
        mercado.setResponsableMercado(responsableNuevo);
        assertEquals("Natalia",mercado.getResponsableMercado().getnombreResponsable());
        assertEquals("Olivera",mercado.getResponsableMercado().getApellidoResponsable());
    }

    //-------------SECTORES---------------
    @Test public void agregarSector() {
        Sector sector1 = new Sector(134,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Sector sector2 = new Sector(23,true, new BigDecimal(0.20),new BigDecimal(500), new BigDecimal(0.20));;
        mercado.agregarSector(sector1);
        mercado.agregarSector(sector2);
        assertEquals(sector2,mercado.encontrarSector(23));
    }

    @Test public void encontrarSector() {
        Sector sector1 = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Sector sector2 = new Sector(2,true, new BigDecimal(0.20),new BigDecimal(500), new BigDecimal(0.20));
        Sector sector3 = new Sector(3,true, new BigDecimal(0.15),new BigDecimal(500), new BigDecimal(0.15));
        mercado.agregarSector(sector1);
        mercado.agregarSector(sector2);
        mercado.agregarSector(sector3);
        Sector sectorEncontrado;
        sectorEncontrado = mercado.encontrarSector(3);
        assertEquals((Integer) 3,(Integer) sectorEncontrado.getNumeroSector());
    }

    @Test public void habilitarSector() {
        Sector sector2 = new Sector(2,false, new BigDecimal(0.20),new BigDecimal(500), new BigDecimal(0.20));
        mercado.agregarSector(sector2);
        mercado.habilitarSector(2);        
        assertEquals(true,sector2.getHabilitado());
    }

    @Test public void deshabilitarSector() {
        Sector sector2 = new Sector(2,true, new BigDecimal(0.20),new BigDecimal(500), new BigDecimal(0.20));
        mercado.agregarSector(sector2);
        mercado.deshabilitarSector(2);        
        assertEquals(false,sector2.getHabilitado());
    }

    @Test public void eliminarUnSector() {
        Sector sector1 = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Sector sector2 = new Sector(2,true, new BigDecimal(0.20),new BigDecimal(500), new BigDecimal(0.20));
        mercado.agregarSector(sector1);
        mercado.agregarSector(sector2);
        mercado.eliminarUnSector(2);
        assertEquals((Integer) 1,(Integer) mercado.getCantidadSectores());
    }


    //-----------------CLIENTE--------------------
    @Test 
    public void agregarUnCliente(){
        Cliente clienteNuevo= new Quintero(123,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        mercado.agregarClientes(clienteNuevo);
        assertEquals(clienteNuevo,mercado.encontrarCliente(123));
    }

    @Test 
    public void buscarUnCliente(){
        Cliente clienteABuscar = new Quintero(123,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        mercado.agregarClientes(clienteABuscar);
        Cliente clienteEncontrado = mercado.encontrarCliente(123);
        assertEquals((Integer) 123,(Integer) clienteEncontrado.getNumeroDeCliente());
    }

    @Test 
    public void eliminarUnCliente(){
        Cliente cliente = new Quintero(123,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        mercado.agregarClientes(cliente);
        mercado.eliminarCliente(cliente);
        assertEquals((Integer) 0,(Integer)mercado.getCantidadClientes());
    }

    @Test 
    public void modificarUncliente(){
        Cliente clienteViejo = new Quintero(123,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        mercado.agregarClientes(clienteViejo);
        Cliente clienteNuevo = new Empresa(111, "Empresa Comercial", "12345678987", "domicilio", "1234567890", "empresa@gmail.com");
        mercado.modificarCliente(clienteViejo, clienteNuevo);
    }


    //---------------CONTRATOS-----------------
    @Test
    public void agregarContratosYEncontrarUnContrato(){
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new  Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        Puesto puesto2 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        Puesto puesto3 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        sector.agregarPuesto(puesto1);
        sector.agregarPuesto(puesto2);
        sector.agregarPuesto(puesto3);
        Contrato contrato1 = new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto1,cliente,responsable);
        Contrato contrato2 = new Contrato(2,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto2,cliente,responsable);
        Contrato contrato3 = new Contrato(3,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto3,cliente,responsable);
        mercado.agregarContratos(contrato1);
        mercado.agregarContratos(contrato2);
        mercado.agregarContratos(contrato3);
        Contrato contratoEncontrado = mercado.encontrarContrato(2);
        assertEquals(contrato2,contratoEncontrado);
    }

    @Test
    public void darDeBajaContrato(){
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new  Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        Puesto puesto2 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        Puesto puesto3 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        sector.agregarPuesto(puesto1);
        sector.agregarPuesto(puesto2);
        sector.agregarPuesto(puesto3);
        Contrato contrato1 = new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto1,cliente,responsable);
        Contrato contrato2 = new Contrato(2,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto2,cliente,responsable);
        Contrato contrato3 = new Contrato(3,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto3,cliente,responsable);
        mercado.agregarContratos(contrato1);
        mercado.agregarContratos(contrato2);
        mercado.agregarContratos(contrato3);
        mercado.darDeBajaContrato(2);
        assertEquals(false,contrato2.getVigente());
    
    }

    @Test 
    public void verificarQueSeElimineUnContrato(){
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new  Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        Puesto puesto2 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        Puesto puesto3 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        sector.agregarPuesto(puesto1);
        sector.agregarPuesto(puesto2);
        sector.agregarPuesto(puesto3);
        Contrato contrato1 = new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto1,cliente,responsable);
        Contrato contrato2 = new Contrato(2,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto2,cliente,responsable);
        Contrato contrato3 = new Contrato(3,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto3,cliente,responsable);
        mercado.agregarContratos(contrato1);
        mercado.agregarContratos(contrato2);
        mercado.agregarContratos(contrato3);
        mercado.darDeBajaContrato(1);
        mercado.eliminarContrato(contrato1);
        assertEquals((Integer) 2,(Integer) mercado.getCantidadContratos());
    }

    @Test
    public void modificarUnContrato(){
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new  Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        Puesto puesto2 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        Puesto puesto3 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        sector.agregarPuesto(puesto1);
        sector.agregarPuesto(puesto2);
        sector.agregarPuesto(puesto3);
        Contrato contrato1 = new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto1,cliente,responsable);
        Contrato contrato2 = new Contrato(2,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto2,cliente,responsable);
        Contrato contrato3 = new Contrato(3,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto3,cliente,responsable);
        mercado.agregarContratos(contrato1);
        mercado.agregarContratos(contrato2);
        mercado.modificarContrato(contrato1, contrato3);
        Contrato contratoEncontrado = mercado.encontrarContrato(3);
        assertEquals(contrato3,contratoEncontrado);
        assertEquals((Integer) 2,(Integer) mercado.getCantidadContratos());
    }

    public void buscarContratosDentroDeUnPeriodoDeTiempo(){
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new  Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        Puesto puesto2 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        Puesto puesto3 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        sector.agregarPuesto(puesto1);
        sector.agregarPuesto(puesto2);
        sector.agregarPuesto(puesto3);
        Contrato contrato1 = new Contrato(1,LocalDate.parse("2020-03-01"),LocalDate.parse("2020-09-24"),puesto1,cliente,responsable);
        Contrato contrato2 = new Contrato(2,LocalDate.parse("2020-02-01"),LocalDate.parse("2020-10-24"),puesto2,cliente,responsable);
        Contrato contrato3 = new Contrato(3,LocalDate.parse("2021-01-01"),LocalDate.parse("2021-03-24"),puesto3,cliente,responsable);
        mercado.agregarContratos(contrato1);
        mercado.agregarContratos(contrato2);
        mercado.agregarContratos(contrato3);
        ArrayList<Contrato> contratosEncontrados = mercado.encontrarContratosEnUnPeriodo(LocalDate.parse("2020-01-01"),LocalDate.parse("2020-12-31"));
        assertEquals(2,contratosEncontrados.size());
    }

    //---------------USUARIOS-----------------

    @Test 
    public void agregarUsuarioYBuscarUsuario(){
        Usuario usuario= new Usuario("Olivera","Natalia","40598438","natalia","abc123");
        mercado.agregarUsuario(usuario);
        assertEquals(usuario, mercado.buscarUsuario("natalia","abc123"));
    }

    @Test 
    public void eliminarUsuarios(){
        Usuario usuario1= new Usuario("Olivera","Natalia","40598438","natalia","abc123");
        Usuario usuario2 = new Usuario("Pereyra","Evelyn","40296986","evelyn","contasenia123");
        mercado.agregarUsuario(usuario1);
        mercado.agregarUsuario(usuario2);
        mercado.eliminarUsuario(usuario1);
        assertEquals((Integer) 1,(Integer) mercado.getCantidadUsuarios());
    }

    @Test 
    public void modificarUsuario(){
        Usuario usuario1= new Usuario("Olivera","Natalia","40598438","natalia","abc123");
        Usuario usuario2 = new Usuario("Pereyra","Evelyn","40296986","evelyn","contasenia123");
        mercado.agregarUsuario(usuario1);
        mercado.modificarUsuario(usuario1, usuario2);
        assertEquals((Integer) 1,(Integer) mercado.getCantidadUsuarios());
        assertEquals(usuario2, mercado.buscarUsuario("evelyn","contrasenia123"));
    }


    //------------EXCEPCIONES------------
    @Test (expected = SectorExistenteException.class)
        public void agregarSectorExistente(){
        Sector sector1 = new Sector(123,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        mercado.agregarSector(sector1);  
        mercado.agregarSector(sector1);
    }

    @Test (expected = CapacidadDeSectoresSuperada.class)
    public void agregarSectorSuperadaLaCapacidadMaxima(){
        Sector sector1 = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Sector sector2 = new Sector(2,false, new BigDecimal(0.20),new BigDecimal(500), new BigDecimal(0.20));
        Sector sector3 = new Sector(3,true, new BigDecimal(0.15),new BigDecimal(500), new BigDecimal(0.15));
        Sector sector4 = new Sector(4,true, new BigDecimal(0.15),new BigDecimal(500), new BigDecimal(0.15));
        mercado.agregarSector(sector1);
        mercado.agregarSector(sector2);
        mercado.agregarSector(sector3);
        mercado.agregarSector(sector4);
    }

    @Test (expected = SectorInexistenteException.class)
    public void eliminarSectorInexistente(){
    mercado.eliminarUnSector(1234);
    }

    @Test (expected = SectorInexistenteException.class)
    public void habilitarSectorInexistente(){
    mercado.habilitarSector(123);
    }

    @Test (expected = SectorInexistenteException.class)
    public void deshabilitarSectorInexistente(){
    mercado.deshabilitarSector(123);
    }

    @Test (expected = SectorYaHabilitadoException.class)
    public void habilitarSectorYaHabilitado(){
        Sector sector1 = new Sector(123,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        mercado.agregarSector(sector1);
        mercado.habilitarSector(123);
    }

    @Test (expected = SectorYaDeshabilitadoException.class)
    public void deshabilitarSectorYaDeshabilitado(){
        Sector sector1 = new Sector(123,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        mercado.agregarSector(sector1);
        mercado.deshabilitarSector(123);
        mercado.deshabilitarSector(123);
    }

    @Test (expected = ContratoNoVigenteException.class)
    public void darDeBajaContratoYaDadoDeBaja(){
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        sector.agregarPuesto(puesto1);
        Contrato contrato = new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto1,cliente,responsable);
        mercado.agregarContratos(contrato);
        mercado.darDeBajaContrato(1);
        mercado.darDeBajaContrato(1);
    }

    @Test (expected = EliminarContratoVigenteException.class)
    public void noPermitirEliminarUnContratoQueAunEsteVigente(){
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        sector.agregarPuesto(puesto1);
        Contrato contrato1 = new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto1,cliente,responsable);
        mercado.agregarContratos(contrato1);
        mercado.eliminarContrato(contrato1);
    }

    @Test (expected = UsuarioExistenteException.class)
    public void noPermitirAgregarElMismoUsuarioMasDeUnaVez(){
        Usuario usuario1= new Usuario("Olivera","Natalia","40598438","natalia","abc123");
        Usuario usuario2= new Usuario("Olivera","Soledad","40598432","natalia","abc123");
        mercado.agregarUsuario(usuario1);
        mercado.agregarUsuario(usuario2);
    }

    @Test (expected = EmpleadoRegistradoException.class)
    public void verificarQueElEmpleadoNoTengaDosCuentas(){
        Usuario usuario1= new Usuario("Olivera","Natalia","40598438","natalia","abc123");
        Usuario usuario2= new Usuario("Olivera","Soledad","40598438","nati","abc123");
        mercado.agregarUsuario(usuario1);
        mercado.agregarUsuario(usuario2);
    }

    @Test (expected = UsuarioInexistenteException.class)
    public void buscarUnUsuarioQueNoExiste(){
        mercado.buscarUsuario("asd","123");
    }

    @Test (expected = ContratoInexistenteException.class)
    public void buscarContratoInexistente(){
        mercado.encontrarContrato(123);
    }

    @Test (expected = ContratoExistenteException.class)
    public void agregarContratoYaExistente(){
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new Medidor(45);
        Puesto puesto1 = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor, sector);
        sector.agregarPuesto(puesto1);
        Contrato contrato = new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto1,cliente,responsable);
        mercado.agregarContratos(contrato);
        mercado.agregarContratos(contrato);
    }

    @Test (expected = ClienteExistenteException.class)
    public void controlarQueNoSeAgregueElMismoCliente(){
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Cliente cliente2 = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        mercado.agregarClientes(cliente);
        mercado.agregarClientes(cliente2);
    }

    @Test (expected = ClienteInexistenteException.class)
    public void clienteNoEntontrado(){
        mercado.encontrarCliente(1);
    }
  
}
