package mercado.facturacion;

import org.junit.Test;
import java.time.LocalDate;
import java.math.BigDecimal;
import java.time.YearMonth;
import static org.junit.Assert.*;

import mercado.mercado.*;
import mercado.clientes.*;
import mercado.excepciones.*;

public class FacturaTest {

    @Test
    public void calcularPrecioTotalAPagar(){
        Mercado mercado = new Mercado("Abasto","Av.Ocampo",4700,38345358,4);
        ResponsableMercado responsable = new ResponsableMercado("Evelyn", "Pereyra","16342845",mercado);
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new Medidor(45);
        Puesto puesto = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor,sector);
        sector.agregarPuesto(puesto);
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Contrato contrato = new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto,cliente,responsable);
        LecturaMedidor lecturaMedidor = new LecturaMedidor(YearMonth.parse("2020-12"),new BigDecimal(234) ,new BigDecimal(13),83);
        Factura factura1 = new Factura(1,LocalDate.parse("2020-10-01"),responsable, cliente,"Descripcion");
        factura1.agregarConceptos(contrato);
        factura1.agregarConceptos(lecturaMedidor);   
        BigDecimal precioTotal = factura1.getTotalAPagar();
        assertEquals(new BigDecimal(17292),precioTotal);
    }

    @Test (expected = ConceptoYaCargadoException.class)
    public void verificarQueNoSecargueElMismoConcepto(){
        Mercado mercado = new Mercado("Abasto","Av.Ocampo",4700,38345358,4);
        ResponsableMercado responsable = new ResponsableMercado("Evelyn", "Pereyra","16342845",mercado);
        Sector sector = new Sector(1,true, new BigDecimal(0.30),new BigDecimal(500), new BigDecimal(0.30));
        Medidor medidor = new Medidor(45);
        Puesto puesto = new Puesto(123,true,new BigDecimal(15),true,true,"comentario",medidor,sector);
        sector.agregarPuesto(puesto);
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
        Contrato contrato = new Contrato(1,LocalDate.parse("2020-10-01"),LocalDate.parse("2020-12-24"),puesto,cliente,responsable);
        LecturaMedidor lecturaMedidor = new LecturaMedidor(YearMonth.parse("2020-12"),new BigDecimal(234) ,new BigDecimal(13),83);
        Factura factura1 = new Factura(1,LocalDate.parse("2020-10-01"),responsable, cliente,"Descripcion");
        factura1.agregarConceptos(contrato);
        factura1.agregarConceptos(contrato);   
        BigDecimal precioTotal = factura1.getTotalAPagar();
    }
    
    
}
