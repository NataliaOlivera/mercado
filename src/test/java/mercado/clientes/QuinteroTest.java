package mercado.clientes;

import org.junit.Test;

import mercado.excepciones.*;

public class QuinteroTest {

    @Test (expected = ErrorAlIngresarCuilException.class)
    public void crearClienteQuinteroConNumeroDeCuilNoValido(){
        Cliente cliente = new Quintero(1,"Natalia Olivera", "271234230", "domicilio", "0303456000", "nati@gmail.com");
    }
    
}
