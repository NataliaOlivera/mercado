package mercado.clientes;

import org.junit.Test;
import org.junit.*;

import mercado.mercado.*;
import mercado.excepciones.*;

public class ClienteTest {
    ResponsableMercado responsable;
    Mercado mercado;
    @Before
    public void initObjects(){
        mercado = new Mercado("Abasto","Av.Ocampo",4700,38345358,4);
        responsable = new ResponsableMercado("Evelyn", "Pereyra","16342845",mercado);
    }

    ////////////////////EXCEPCIONES///////////////
    @Test (expected = ErrorAlIngresarRazonSocialException.class)
    public void crearClienteConRazonSocialNoValida(){
        Cliente cliente = new Quintero(1,"Natalia99 Olivera","27123423000", "domicilio", "0303456000", "nati@gmail.com");
    }

    @Test (expected = TelefonoMalIngresadoException.class)
    public void crearClienteConNumeroTelefonicoNoValido(){
        Cliente cliente = new Quintero(1,"Natalia Olivera","27123423000", "domicilio", "0303456", "nati@gmail.com");
    }
}
