package mercado.clientes;

import org.junit.Test;

import mercado.excepciones.*;

public class EmpresaTest {

    @Test (expected = ErrorAlIngresarCuitException.class)
    public void crearClienteEmpresaConNumeroDeCuitNoValido(){
        Cliente cliente = new Empresa(344, "Empresa", "34t", "domicilio", "0303446000", "empresa@gmail.com");
    }
    
}
