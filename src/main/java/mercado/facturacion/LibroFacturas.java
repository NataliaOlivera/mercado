package mercado.facturacion;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

import mercado.excepciones.*;

public class LibroFacturas {
    private ArrayList<Factura> facturas;

    public LibroFacturas(){
        facturas = new ArrayList<Factura>();
    }

    public ArrayList<Factura> getFacturas() {
        return facturas;
    }


    public Integer getCantidadFacturas(){
        return facturas.size();
    }

    public void agregarFactura(Factura factura) {
            for (Factura aux : facturas) {
                if (aux.equals(factura)) {
                  throw new FacturaExistenteException();
                }  
            }
            this.facturas.add(factura);
    }


    public Factura encontrarFactura(Integer numeroFactura){
        Factura facturaEncontrada = null;
        for (Factura aux : facturas) {
            if (aux.getNumeroFactura().equals(numeroFactura)){
                facturaEncontrada = aux;
                break;
            }
        }
        if (facturaEncontrada ==null){
            throw new FacturaInexistenteException();
        }
        return facturaEncontrada;
    }


    public void eliminarFactura(Integer numeroFactura){
        Factura facturaAEliminar=encontrarFactura(numeroFactura);
        facturas.remove(facturaAEliminar);
    }

    public void modificarFactura(Factura factura, Factura nuevaFactura){
        eliminarFactura(factura.getNumeroFactura());
        agregarFactura(nuevaFactura);
    }

    public ArrayList<Factura> buscarFacturasDeUnMes(Integer numeroDeMes){
        ArrayList<Factura> facturasEncontradas = new ArrayList();
        for (Factura aux : facturas) {
            if (aux.getFechaFacturacion().getMonthValue() == numeroDeMes){
                facturasEncontradas.add(aux);
            }           
        }
        if(facturasEncontradas.isEmpty()){
            throw new FacturasNoEncontradasException();
        }
        return facturasEncontradas;    

    }


    public BigDecimal calcularImporteDeFacturasDeUnMes(Integer numeroDeMes){
        ArrayList<Factura> facturasEncontradas = buscarFacturasDeUnMes(numeroDeMes);
        BigDecimal importeTotal = new BigDecimal (0);
        for (Factura auxiliar : facturasEncontradas){
            importeTotal = importeTotal.add(auxiliar.getTotalAPagar()).setScale(0, RoundingMode.HALF_UP);
        }
        return importeTotal;
    }

    public ArrayList<Factura> encontrarFacturasEnUnPeriodo(LocalDate fecha1,LocalDate fecha2){
        ArrayList<Factura> facturasEncontradas = new ArrayList();
        for (Factura auxiliar : facturas) {
            if ( fecha1.isBefore(auxiliar.getFechaFacturacion()) &&  fecha2.isAfter(auxiliar.getFechaFacturacion())){
                facturasEncontradas.add(auxiliar);
            }
        }
        if (facturasEncontradas.isEmpty()) {
            throw new FacturasNoEncontradasException();
        }
        return facturasEncontradas;
    }
    
  
    public ArrayList<Factura> ordenarFacturasFecha(){
        Collections.sort(facturas, new ComparadorFacturasFechas());
        return facturas;
    }
}