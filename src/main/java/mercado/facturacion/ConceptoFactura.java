package mercado.facturacion;

import java.math.BigDecimal;

public interface ConceptoFactura {
    public BigDecimal getImporteMensual();
    public Integer getCodigoDelConcepto();
}
