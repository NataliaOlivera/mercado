package mercado.facturacion;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

import mercado.mercado.*;
import mercado.clientes.*;
import mercado.excepciones.*;

public class Factura {
    private Integer numeroFactura;

    public void setConceptos(ArrayList<ConceptoFactura> conceptos) {
        this.conceptos = conceptos;
    }
    private LocalDate fechaFacturacion;
    private String descripcion;
    private Cliente cliente;
    private ResponsableMercado responsableMercado;
    private ArrayList<ConceptoFactura> conceptos;

    public Factura(Integer numeroFactura, LocalDate fechaFacturacion, ResponsableMercado responsableMercado,
            Cliente cliente, String descripcion) {
        this.fechaFacturacion = fechaFacturacion;
        this.descripcion = descripcion;
        this.cliente = cliente;
        this.responsableMercado = responsableMercado;
        conceptos = new ArrayList();
        this.numeroFactura = numeroFactura;
    }

    // -------------GETTERS-----------------
    public Integer getNumeroFactura() {
        return numeroFactura;
    }

    public LocalDate getFechaFacturacion() {
        return fechaFacturacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public ResponsableMercado getResponsableMercado() {
        return responsableMercado;
    }

    public ArrayList<ConceptoFactura> getConceptos() {
        return conceptos;
    }

    public BigDecimal getTotalAPagar() {
        BigDecimal totalAPagar = new BigDecimal(0);
        for (ConceptoFactura var : conceptos) {
            totalAPagar = totalAPagar.add(var.getImporteMensual());
        }
        return totalAPagar;
    }

    public void agregarConceptos(ConceptoFactura concepto) {
        for(ConceptoFactura var : conceptos) {
            if (var.getCodigoDelConcepto().equals(concepto.getCodigoDelConcepto())) {
                throw new ConceptoYaCargadoException();
            }
        }
        conceptos.add(concepto);
    }

    
}