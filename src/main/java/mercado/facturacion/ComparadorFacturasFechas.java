
package mercado.facturacion;

import java.util.Comparator;

public class ComparadorFacturasFechas implements Comparator<Factura> {
    @Override
            public int compare (Factura factura1,Factura factura2){
                        return factura1.getFechaFacturacion().compareTo(factura2.getFechaFacturacion());
                                }
    
}



