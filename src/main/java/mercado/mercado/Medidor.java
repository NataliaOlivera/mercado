package mercado.mercado;

import java.time.YearMonth;
import java.util.ArrayList;
import mercado.excepciones.*;

public class Medidor {
    private ArrayList<LecturaMedidor> lecturas;
    private Integer numeroMedidor;
       
    public Medidor(Integer numeroMedidor) {
        this.numeroMedidor=numeroMedidor;
        lecturas = new ArrayList<LecturaMedidor>();
    }
    
    public void setNumeroMedidor(Integer numeroMedidor) {
        this.numeroMedidor = numeroMedidor;
    }

    //--------GETTERS-----------
    public int getNumeroMedidor() {
        return numeroMedidor;
    }
    public ArrayList<LecturaMedidor> getLecturas() {
        return lecturas;
    }
    
  
    public void agregarLectura(LecturaMedidor lecturaNueva){
            for (LecturaMedidor auxiliar : lecturas) {
                if (auxiliar.equals(lecturaNueva)) {
                  throw new LecturaExistenteException();
                }  
            }
            lecturas.add(lecturaNueva);
    }

    public LecturaMedidor buscarLectura(YearMonth periodoLectura){
        LecturaMedidor lecturaMedidorEncontrada = null;
        for (LecturaMedidor auxiliar : lecturas){
            if(auxiliar.getFinPeriodoConsumo().equals(periodoLectura)){
                lecturaMedidorEncontrada = auxiliar;
                break;    
            }
        }
        if (lecturaMedidorEncontrada== null) {
            throw new LecturaInexistenteException();
        }
        return lecturaMedidorEncontrada;
    }  
 
    public void eliminarLectura(YearMonth periodoLectura){
        LecturaMedidor lecturaAEliminar= buscarLectura(periodoLectura);
        lecturas.remove(lecturaAEliminar);
    }

    public void modificarLectura(LecturaMedidor lectura, LecturaMedidor lecturaNueva){
        eliminarLectura(lectura.getFinPeriodoConsumo());
        agregarLectura(lecturaNueva);
    }
    
    public Integer getCantidadLecturas(){
        return lecturas.size();
    }
    
    
}
