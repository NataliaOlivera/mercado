package mercado.mercado;

import java.time.LocalDate;
import java.util.ArrayList;

import mercado.clientes.*;
import mercado.facturacion.*;
import mercado.excepciones.*;

public class Mercado{
    private String nombreMercado;
    private String direccionMercado;
    private Integer codigoPostal;
    private Integer telefonoMercado;
    private Integer capacidadMaximaDeSectores;
    private ResponsableMercado responsableMercado;
    private ArrayList<Sector> sectores;
    private ArrayList<Contrato> contratos;
    private ArrayList<Cliente> clientes;
    private LibroFacturas registroFacturas;
    private ArrayList<Usuario> usuarios;
   

    public Mercado(String nombreMercado, String direccionMercado, Integer codigoPostal, Integer telefonoMercado,Integer capacidadMaximaDeSectores) {
        this.nombreMercado = nombreMercado;
        this.direccionMercado = direccionMercado;
        this.codigoPostal = codigoPostal;
        this.telefonoMercado = telefonoMercado;
        this.capacidadMaximaDeSectores =capacidadMaximaDeSectores;
        sectores= new ArrayList<Sector>();
        contratos= new ArrayList<Contrato>();
        clientes= new ArrayList<Cliente>();
        usuarios= new ArrayList<Usuario>();
        this.registroFacturas= new LibroFacturas();
    }

    //-------------GETTERS--------------
    public String getNombreMercado() {
        return nombreMercado;
    }
    public ArrayList<Sector> getSectores() {
        return sectores;
    }
    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }
    public LibroFacturas getRegistroFacturas() {
        return registroFacturas;
    }
    public String getDireccionMercado() {
        return direccionMercado;
    }
    public Integer getCodigoPostal() {
        return codigoPostal;
    }
    public Integer getCantidadSectores(){
        return sectores.size();
    }
    public Integer getTelefonoMercado() {
        return telefonoMercado;
    }
    public ResponsableMercado getResponsableMercado() {
        return responsableMercado;
    }
    public ArrayList<Contrato> getContratos() {
        return contratos;
    }
    public ArrayList<Cliente> getClientes() {
        return clientes;
    }
    public Integer getCantidadClientes(){
        return clientes.size();
    }
    public Integer getCantidadContratos(){
        return contratos.size();
    }
    public Integer getCantidadUsuarios(){
        return usuarios.size();
    }
    public Integer getCapacidadMaximaDeSectores() {
        return capacidadMaximaDeSectores;
    }

     //------------SETTERS--------------
     public void setResponsableMercado(ResponsableMercado responsableMercado){
        this.responsableMercado = responsableMercado;
    }
    public void setSectores(ArrayList<Sector> sectores) {
        this.sectores = sectores;
    }
    public void setNombreMercado(String nombreMercado) {
        this.nombreMercado = nombreMercado;
    }
    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    public void setDireccionMercado(String direccionMercado) {
        this.direccionMercado = direccionMercado;
    }
    public void setTelefonoMercado(Integer telefonoMercado) {
        this.telefonoMercado = telefonoMercado;
    }
    public void setCapacidadMaximaDeSectores(Integer capacidadMaximaDeSectores) {
        this.capacidadMaximaDeSectores = capacidadMaximaDeSectores;
    } 
   


   

    //----------MANEJO DE SECTORES----------
    public void agregarSector(Sector sector){ 
        if (getCantidadSectores()< capacidadMaximaDeSectores){
            for (Sector auxiliar : sectores) {
                if (auxiliar.equals(sector)) {
                  throw new SectorExistenteException();
                }  
            }
            this.sectores.add(sector);
        }
        else{
            throw new CapacidadDeSectoresSuperada();
        }
    }

    public Sector encontrarSector(Integer numeroSector){
        Sector sectorEncontrado = null;
        for (Sector var : sectores) {
            if (var.getNumeroSector().equals(numeroSector)) {
                sectorEncontrado = var;
                break;
            }
        }
        if (sectorEncontrado == null) {
            throw new SectorInexistenteException();
        }
        return sectorEncontrado;
    }

    public void habilitarSector(Integer numeroSector){
        Sector sectorAModificar = encontrarSector(numeroSector);
        if (sectorAModificar.getHabilitado()==true){
            throw new SectorYaHabilitadoException();
        }
        sectorAModificar.setHabilitado();
    }

    public void deshabilitarSector(Integer numeroSector){
        Sector sectorAModificar = encontrarSector(numeroSector);
        if (sectorAModificar.getHabilitado()==false){
            throw new SectorYaDeshabilitadoException();
        }
        sectorAModificar.setHabilitado();
    }


    public void eliminarUnSector(Integer numeroSector){
        Sector sectorAEliminar = encontrarSector(numeroSector); 
        sectores.remove(sectorAEliminar);
    }

   

    //----------MANEJO CONTRATOS----------
    public void agregarContratos(Contrato contrato) {
        for(Contrato var: contratos){
            if(var.getNumeroDeContrato().equals(contrato.getNumeroDeContrato())){
                throw new ContratoExistenteException();
            }
        }
        contratos.add(contrato);
        
    }

    public ArrayList<Contrato> encontrarContratosEnUnPeriodo(LocalDate fechaInicio,LocalDate fechaFinal){
        ArrayList<Contrato> contratosEncontrados = new ArrayList<Contrato>();
        for (Contrato auxiliar : contratos) {
            if (fechaInicio.isBefore(auxiliar.getFechaDeInicioAlquiler()) && fechaFinal.isAfter(auxiliar.getFechaFinalAlquiler())) {
                contratosEncontrados.add(auxiliar);
            }
        }
        if (contratosEncontrados.size() == 0) {
            throw new ContratosNoEncontradosException();
        }
        return contratosEncontrados;
    }

    public Contrato encontrarContrato(Integer numeroContrato){
        Contrato contratoEncontrado = null;
        for (Contrato auxiliar : contratos) {
            if (auxiliar.getNumeroDeContrato() == numeroContrato) {
                contratoEncontrado = auxiliar;
                break;
            }
        }
        if (contratoEncontrado == null) {
            throw new ContratoInexistenteException();
        }
        return contratoEncontrado;
    }

    public void eliminarContrato(Contrato contrato){
        Contrato contratoAEliminar= encontrarContrato(contrato.getNumeroDeContrato());
        if(contratoAEliminar.getVigente().equals(true)){
            throw new EliminarContratoVigenteException();
        }
        contratos.remove(contratoAEliminar);
    }

    public void modificarContrato(Contrato contratoViejo, Contrato contratoNuevo){
        Contrato contratoAModificar = encontrarContrato(contratoViejo.getNumeroDeContrato());
        contratos.remove(contratoAModificar);
        agregarContratos(contratoNuevo);
    }

    public void darDeBajaContrato(Integer numeroContrato){
        Contrato contratoADarDeBaja = encontrarContrato(numeroContrato);
        if (contratoADarDeBaja.getVigente()==false){
            throw new ContratoNoVigenteException();
        }
        contratoADarDeBaja.setVigente();
        Puesto puesto = contratoADarDeBaja.getPuesto();
        puesto.getSector().habilitarPuesto(puesto.getNumeroPuesto());
    }

   
    

    //----------MANEJO CLIENTES----------

    public void agregarClientes(Cliente cliente) {
        for(Cliente auxiliar: clientes){
            if(auxiliar.getNumeroDeCliente().equals(cliente.getNumeroDeCliente())){
                throw new ClienteExistenteException();
            }
        }
        clientes.add(cliente);
    }

    public Cliente encontrarCliente(Integer numeroDeCliente){
        Cliente clienteEncontrado= null;
        for(Cliente auxiliar : clientes){
            if(auxiliar.getNumeroDeCliente() ==numeroDeCliente){
                clienteEncontrado=auxiliar;
                break;
            }
        }
        if(clienteEncontrado== null){
            throw new ClienteInexistenteException();
        }
        return clienteEncontrado;
    }

    public void modificarCliente(Cliente clienteViejo, Cliente clienteNuevo){
        eliminarCliente(clienteViejo);
        agregarClientes(clienteNuevo);
    }

    public void eliminarCliente(Cliente cliente){
       Cliente clienteAEliminar= encontrarCliente(cliente.getNumeroDeCliente());
       clientes.remove(clienteAEliminar);
    }


    //----------MANEJO USUARIOS----------

    public void agregarUsuario(Usuario usuario){
        for(Usuario var: usuarios){
            if(var.getDniDelEmpleado().equals(usuario.getDniDelEmpleado())){
                throw new EmpleadoRegistradoException();
            }
            if(var.getNombreUsuario().equals(usuario.getNombreUsuario())){
                throw new UsuarioExistenteException();
            }
        }
        usuarios.add(usuario);
    }
    
    public Usuario buscarUsuarioPorDNIDelEmpleado(String dni){
        Usuario usuarioEncontrado=null;
        for(Usuario var: usuarios){
            if(var.getDniDelEmpleado().equals(dni)){
                usuarioEncontrado=var;
                break;
            }
        }
        if(usuarioEncontrado== null){
            throw new UsuarioInexistenteException();
        }
        return usuarioEncontrado;
    }

    public Usuario buscarUsuario(String nombreUsuario, String contrasenia){
        Usuario usuarioEncontrado=null;
        for(Usuario var: usuarios){
            if(var.getNombreUsuario().equals(nombreUsuario)){
                usuarioEncontrado=var;     
                if(usuarioEncontrado.getContrasenia().equals(contrasenia)){
                    break;
                }
                else{
                    throw new ContraseniaIncorrectaException();
                }
            }
        }
        if(usuarioEncontrado== null){
            throw new UsuarioInexistenteException();
        }
        return usuarioEncontrado;
    }

    public void eliminarUsuario(Usuario usuario){
        Usuario usuarioAEliminar=buscarUsuario(usuario.getNombreUsuario(), usuario.getContrasenia());
        usuarios.remove(usuarioAEliminar);
    }

    public void modificarUsuario(Usuario usuarioViejo, Usuario usuarioNuevo){
        eliminarUsuario(usuarioViejo);
        agregarUsuario(usuarioNuevo);
    }


   


}