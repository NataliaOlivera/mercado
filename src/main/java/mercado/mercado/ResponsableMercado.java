package mercado.mercado;

import java.util.regex.*;

import mercado.excepciones.*;

public class ResponsableMercado {
    private String nombreResponsable;
    private String apellidoResponsable;
    private String dni;
    private Mercado mercado;

    public ResponsableMercado(String nombreResponsable, String apellidoResponsable, String  dni,Mercado mercado) {
	controlarIngresoDelNombre(nombreResponsable);
	controlarIngresoDelApellido(apellidoResponsable);
	controlarElIngresoDelCuit(dni);
	this.nombreResponsable=nombreResponsable;
                   this.apellidoResponsable=apellidoResponsable;
                   this.dni = dni;
	this.mercado = mercado;
    }
	
	//--------GETTERS--------
    public Mercado getMercado(){
        return mercado;
    }
    public String getnombreResponsable() {
		return nombreResponsable;
    }
    public String getApellidoResponsable() {
	return apellidoResponsable;
    }
    public String getDni() {
	return dni;
    }


	//---------SETTERS-------
    public void setDni(String dni) {
        controlarElIngresoDelCuit(dni);
	this.dni = dni;
    }
    public void setnombreResponsable(String nombreResponsable) {
	controlarIngresoDelNombre(nombreResponsable);
	this.nombreResponsable = nombreResponsable;
    }
	
    public void setApellidoResponsable(String apellidoResponsable) {
        controlarIngresoDelApellido(apellidoResponsable);
	this.apellidoResponsable = apellidoResponsable;
    }


///////////////////////////////CONTROL DE INGRESO DE DATOS////////////////////////////

    private void controlarIngresoDelNombre(String nombreResponsable){
        Pattern pat= Pattern.compile("^([a-zA-z\\s\\s\\s]{0,30})$");
	Matcher mat=pat.matcher(nombreResponsable);
	if(mat.find()==false){
		throw new CaracteresNoValidosEnNombreException();
	}
    }

    private void controlarIngresoDelApellido(String apellidoResponsable){
	Pattern pat= Pattern.compile("^([a-zA-z\\s\\s\\s]{0,30})$");
	Matcher mat=pat.matcher(apellidoResponsable);
	if(mat.find()==false){
		throw new CaracteresNoValidosEnApellidoException();
	}
    }

    private void controlarElIngresoDelCuit(String dni){
        Pattern pat= Pattern.compile("^([0-9]{8})$");
        Matcher mat=pat.matcher(dni);
        if(mat.find()==false){
            throw new CaracteresNoValidosEnDNIException();
        }
    }

}
