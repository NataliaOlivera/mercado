package mercado.mercado;

import mercado.excepciones.*;
import java.util.regex.*;

public class Usuario {
    private String apellidoDelEmpleado;
    private String nombreDelEmpleado;
    private String dniDelEmpleado;
    private String nombreUsuario;
    private String contrasenia;

    public Usuario(String apellidoDelEmpleado, String nombreDelEmpleado, String dniDelEmpleado, String nombreUsuario, String contrasenia) {
        this.apellidoDelEmpleado=apellidoDelEmpleado;
        this.nombreDelEmpleado = nombreDelEmpleado;
        this.dniDelEmpleado = dniDelEmpleado;
        this.nombreUsuario = nombreUsuario;
        this.contrasenia = contrasenia;
        controlarElIngresoDelDni(dniDelEmpleado);
        controlarIngresoDelApellidoDelEmpleado(apellidoDelEmpleado);
        controlarIngresoDelnombreDelEmpleado(nombreDelEmpleado);
    }

    public String getApellidoDelEmpleado() {
        return apellidoDelEmpleado;
    }

    public void setApellidoDelEmpleado(String apellidoDelEmpleado) {
        controlarIngresoDelApellidoDelEmpleado(apellidoDelEmpleado);
        this.apellidoDelEmpleado = apellidoDelEmpleado;
    }

    public String getNombreDelEmpleado() {
        return nombreDelEmpleado;
    }

    public void setNombreDelEmpleado(String nombreDelEmpleado) {
        controlarIngresoDelnombreDelEmpleado(nombreDelEmpleado);
        this.nombreDelEmpleado = nombreDelEmpleado;
    }

    public String getDniDelEmpleado() {
        return dniDelEmpleado;
    }

    public void setDniDelEmpleado(String dniDelEmpleado) {
        controlarElIngresoDelDni(dniDelEmpleado);
        this.dniDelEmpleado = dniDelEmpleado;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    ///////////////////////////////CONTROL DE INGRESO DE DATOS////////////////////////////

    private static void controlarElIngresoDelDni(String dniDelEmpleado){
        Pattern pat= Pattern.compile("^([0-9]{8})$");
        Matcher mat=pat.matcher(dniDelEmpleado);
        if(mat.find()==false){
            throw new ErrorAlIngresarDNIException();
        }
    }

    private static void controlarIngresoDelApellidoDelEmpleado(String apellidoDelEmpleado){
        Pattern pat= Pattern.compile("^([a-zA-z\\s\\s]{0,25})$");
        Matcher mat=pat.matcher(apellidoDelEmpleado);
        if(mat.find()==false){
            throw new ErrorAlIngresarApellidoDelEmpleadoException();
        }
    }

    private static void controlarIngresoDelnombreDelEmpleado(String nombreDelEmpleado){
        Pattern pat= Pattern.compile("^([a-zA-z\\s\\s]{0,25})$");
        Matcher mat=pat.matcher(nombreDelEmpleado);
        if(mat.find()==false){
            throw new ErrorAlIngresarNombreDelEmpleadoException();
        }
    }
    
    
}
