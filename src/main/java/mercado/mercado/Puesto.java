package mercado.mercado;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Puesto {
    
    private Integer numeroPuesto;
    private BigDecimal dimension;
    private Boolean disponible;
    private Boolean camaraFrigorifica;
    private Boolean techo;
    private String comentario;
    private Sector sector;
    private Medidor medidor;
    

    public Puesto(Integer numeroPuesto,Boolean disponible, BigDecimal dimension,
     Boolean camaraFrigorifica,Boolean techo, String comentario,Medidor medidor, Sector sector) {
        this.numeroPuesto = numeroPuesto;
        this.dimension = dimension;
        this.disponible = disponible;
        this.comentario = comentario;
        this.camaraFrigorifica = camaraFrigorifica;
        this.techo = techo;  
        this.sector=sector;
        this.medidor=medidor;
    }


    //-------GETTERS---------
    public Integer getNumeroPuesto() {
        return numeroPuesto;
    }
    public BigDecimal getDimension() {
        return dimension;
    }
    public Boolean getDisponible() {
        return disponible;
    } 
    public String getComentario() {
        return comentario;
    }
    public Boolean getCamaraFrigorifica() {
        return camaraFrigorifica;
    }
    public Boolean getTecho() {
        return techo;
    } 
    public Medidor getMedidor(){
        return medidor;
    }
    public Sector getSector() {
        return sector;
    }


    //-----SETTERS-----
   
    public void setDisponible(Boolean disponibilidad) {
        if(disponibilidad==true){
            this.disponible=false;
        }else{
            this.disponible=true;
        }
    }
    
    

    
   
    //////////////////////PARA EL CALCULO DE PRECIO ALQUILER///////////////////////

 

    public BigDecimal getPrecioPuesto(){
        BigDecimal precioDimensiones=getSector().getPrecioPuestoPorM2().multiply(dimension).setScale(0, RoundingMode.HALF_UP);
        BigDecimal auxPrecio= new BigDecimal(0);
        BigDecimal auxInteresSector= getSector().getInteresDelSector();

        if(getCamaraFrigorifica().equals(true)){
            auxPrecio=auxPrecio.add(precioDimensiones.multiply(getSector().getInteresDelPuesto())).setScale(0, RoundingMode.HALF_UP);
        }

        if(getTecho().equals(true)){
            auxPrecio=auxPrecio.add(precioDimensiones.multiply(getSector().getInteresDelPuesto())).setScale(0, RoundingMode.HALF_UP);
        }

        auxPrecio=auxPrecio.add(precioDimensiones.multiply(auxInteresSector)).setScale(0, RoundingMode.HALF_UP);;
        
        BigDecimal precioPuesto=precioDimensiones.add(auxPrecio);
        return precioPuesto;
    }   
}
       
