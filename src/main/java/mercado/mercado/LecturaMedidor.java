package mercado.mercado;

import java.math.BigDecimal;
import java.time.YearMonth;

import mercado.facturacion.*;

public class LecturaMedidor implements ConceptoFactura{
    private YearMonth inicioPeriodoConsumo;
    private YearMonth finPeriodoConsumo;
    private BigDecimal consumoMensual;
    private BigDecimal preciokWh;
    private Integer codigoLectura;

    public LecturaMedidor(YearMonth inicioPeriodoConsumo,BigDecimal consumoMensual,BigDecimal preciokWh, Integer codigoLectura){
        this.consumoMensual=consumoMensual;
        this.preciokWh=preciokWh;
        this.inicioPeriodoConsumo=inicioPeriodoConsumo;
        this.finPeriodoConsumo=inicioPeriodoConsumo.plusMonths(1);
        this.codigoLectura=codigoLectura;
    }

    //---------GETTERS----------
     public YearMonth getInicioPeriodoConsumo() {
        return inicioPeriodoConsumo;
    }
    public YearMonth getFinPeriodoConsumo() {
        return finPeriodoConsumo;
    } 
    public BigDecimal getConsumoMensual() {
        return consumoMensual;
    }
    public BigDecimal getPreciokWh() {
        return preciokWh;
    }
    @Override
     public BigDecimal getImporteMensual() {
        BigDecimal importeMensual = preciokWh.multiply(consumoMensual);
        return importeMensual;
    }

    @Override
    public Integer getCodigoDelConcepto() {
        return codigoLectura;
    }

    
    
    //---------SETTERS-----------
    public void setPreciokWh(BigDecimal preciokWh) {
        this.preciokWh = preciokWh;
    }

    public Integer getCodigoLectura() {
        return codigoLectura;
    }
    public void setInicioPeriodoConsumo(YearMonth inicioPeriodoConsumo) {
        this.inicioPeriodoConsumo = inicioPeriodoConsumo;
    }

    public void setFinPeriodoConsumo(YearMonth finPeriodoConsumo) {
        this.finPeriodoConsumo = finPeriodoConsumo;
    }

    public void setConsumoMensual(BigDecimal consumoMensual) {
        this.consumoMensual = consumoMensual;
    }

    public void setCodigoLectura(Integer codigoLectura) {
        this.codigoLectura = codigoLectura;
    }


    


}
