package mercado.mercado;

import java.math.BigDecimal;
import java.util.ArrayList;

import mercado.excepciones.*;

public class Sector {
    private Integer numeroSector;
    private ArrayList<Puesto> puestos;
    private Boolean habilitado;
    private BigDecimal interesDelPuesto;
    private BigDecimal precioPuestoPorM2;
    private BigDecimal interesDelSector;
   
    
    public Sector(Integer numeroSector, Boolean habilitado,BigDecimal interesDelPuesto, BigDecimal precioPuestoPorM2, BigDecimal interesDelSector) {
        this.numeroSector=numeroSector;
        puestos= new ArrayList();
        this.habilitado=habilitado;
        this.interesDelPuesto=interesDelPuesto;
        this.precioPuestoPorM2=precioPuestoPorM2;
        this.interesDelSector=interesDelSector;
    }

    //---------GETTERS------
    public Boolean getHabilitado(){
        return habilitado;
    }
    public ArrayList<Puesto> getPuestos() {
        return puestos;
    }
    public Integer getNumeroSector() {
        return numeroSector;
    }
    public Integer getCantidadPuestos() {
        return puestos.size();
    }
    public BigDecimal getInteresDelPuesto() {
        return interesDelPuesto;
    }
    public BigDecimal getPrecioPuestoPorM2() {
        return precioPuestoPorM2;
    }
    public BigDecimal getInteresDelSector() {
        return interesDelSector;
    }

   
   

    //-------SETTERS----------
   
    public void setInteresDelPuesto(BigDecimal interesDelPuesto) {
        this.interesDelPuesto = interesDelPuesto;
    }
    public void setPrecioPuestoPorM2(BigDecimal precioPuestoPorM2) {
        this.precioPuestoPorM2 = precioPuestoPorM2;
    }
    public void setInteresDelSector(BigDecimal interesDelSector) {
        this.interesDelSector = interesDelSector;
    }
    public void setHabilitado() {
        if(getHabilitado().equals(true)){
            this.habilitado=false;
        }else{
            this.habilitado=true;
        }
    }
    




    //--------MANEJO PUESTOS----------
    public void agregarPuesto(Puesto puesto)throws PuestoExistenteException { 
        for (Puesto auxiliar : puestos) {
           if (auxiliar.equals(puesto)) {
             throw new PuestoExistenteException();
           }  
        }
        this.puestos.add(puesto); 
    }
     
    public Puesto encontrarPuesto(Integer numeroPuesto){
        Puesto puestoEncontrado = null;
        for (Puesto var : puestos) {
            if (var.getNumeroPuesto().equals(numeroPuesto)) {
                puestoEncontrado = var;
                break;
            }
        }
        if (puestoEncontrado == null) {
            throw new PuestoInexistenteException();
        }
        return puestoEncontrado;
    }

    public void habilitarPuesto(Integer numeroPuesto){
        Puesto puestoAModificar = encontrarPuesto(numeroPuesto);
        puestoAModificar.setDisponible(puestoAModificar.getDisponible());
    }

    public void deshabilitarPuesto(Integer numeroPuesto){
        Puesto puestoAModificar = encontrarPuesto(numeroPuesto);
        puestoAModificar.setDisponible(puestoAModificar.getDisponible());
    }

    public void eliminarUnPuesto(Integer numeroPuesto){
        Puesto puestoAEliminar = encontrarPuesto(numeroPuesto);
        if (puestoAEliminar.getDisponible().equals(false)){
            throw new PuestoEnContratoException();
        }
            puestos.remove(puestoAEliminar);
    }   

   
}

   
    