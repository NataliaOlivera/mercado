package mercado.mercado;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

import mercado.clientes.*;
import mercado.facturacion.*;

public class Contrato implements ConceptoFactura{
    private BigDecimal precioAlquiler;
    private LocalDate fechaDeInicioAlquiler;
    private LocalDate fechaFinalAlquiler;
    private Integer numeroDeContrato;
    private Puesto puesto; 
    private Cliente cliente;
    private ResponsableMercado responsableMercado;
    private Boolean vigente;
    
    public Contrato(Integer numeroDeContrato,LocalDate fechaDeInicioAlquiler, LocalDate fechaFinalAlquiler,Puesto puesto,Cliente cliente, ResponsableMercado responsableMercado){
        this.fechaDeInicioAlquiler=fechaDeInicioAlquiler;
        this.fechaFinalAlquiler=fechaFinalAlquiler; 
        this.numeroDeContrato=numeroDeContrato;
        this.puesto=puesto;
        this.cliente=cliente;
        this.responsableMercado=responsableMercado;
        this.vigente=true;
        
    }

    //---------GETTERS------------
    public BigDecimal getPrecioAlquiler() {
        BigDecimal monthsBetween=new BigDecimal(ChronoUnit.MONTHS.between(fechaDeInicioAlquiler.withDayOfMonth(1),fechaFinalAlquiler.withDayOfMonth(1)));
        precioAlquiler=getImporteMensual().multiply(monthsBetween);
        return precioAlquiler;
    }
    public LocalDate getFechaDeInicioAlquiler() {
        return fechaDeInicioAlquiler;
    }
    public LocalDate getFechaFinalAlquiler() {
        return fechaFinalAlquiler;
    }
    @Override
    public BigDecimal getImporteMensual() {
        BigDecimal importeMensual=getPuesto().getPrecioPuesto();
        return importeMensual;
    }
    public Integer getNumeroDeContrato() {
        return numeroDeContrato;
    }
    public Puesto getPuesto() {
      return puesto;
    }
    public Cliente getCliente() {
        return cliente;
    }
    public ResponsableMercado getResponsableMercado() {
	    return responsableMercado;
    }
    public Boolean getVigente() {
        return vigente;
    }

    public int getCantidadMesesDeDuracion(){
        Period meses = Period.between(fechaDeInicioAlquiler,fechaFinalAlquiler);
        int periodo = meses.getMonths();
        return periodo;
    }

    @Override
    public Integer getCodigoDelConcepto(){
        return numeroDeContrato;
    }



    


    //---------SETTERS-----------
    public void setFechaDeInicioAlquiler(LocalDate fechaDeInicioAlquiler) {
        this.fechaDeInicioAlquiler = fechaDeInicioAlquiler;
    }
    public void setFechaFinalAlquiler(LocalDate fechaFinalAlquiler) {
        this.fechaFinalAlquiler = fechaFinalAlquiler;
    }
    public void setVigente() {
        if(getVigente().equals(true)){
            this.vigente=false;
        }else{
            this.vigente=true;
        }
    } 
    
    public void setNumeroDeContrato(Integer numeroDeContrato) {
        this.numeroDeContrato = numeroDeContrato;
    }
}