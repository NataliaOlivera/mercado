package mercado.excepciones;

public class ConceptoYaCargadoException extends RuntimeException{

    public ConceptoYaCargadoException() {
        super("El concepto ya fue cargado");
    }
    
}
