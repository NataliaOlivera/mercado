package mercado.excepciones;

public class FacturaInexistenteException extends RuntimeException {
    public FacturaInexistenteException(){
        super("Factura No encontrada");
    }
}

