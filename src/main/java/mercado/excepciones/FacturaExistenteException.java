package mercado.excepciones;

public class FacturaExistenteException extends RuntimeException {
    public FacturaExistenteException(){
        super("Factura Ya Existente");
    }
}
