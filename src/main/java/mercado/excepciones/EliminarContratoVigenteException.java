package mercado.excepciones;

public class EliminarContratoVigenteException extends RuntimeException {

    public EliminarContratoVigenteException() {
        super("No se puede eliminar un contrato vigente");
    }
    
}
