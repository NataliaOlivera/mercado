package mercado.excepciones;

public class ErrorAlIngresarRazonSocialException extends RuntimeException {

    public ErrorAlIngresarRazonSocialException() {
        super("Se ingreso caracter no valido");
    }
    
}
