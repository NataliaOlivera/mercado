package mercado.excepciones;

public class ErrorAlIngresarCuitException extends RuntimeException {

    public ErrorAlIngresarCuitException() {
        super("Se ingreso un cuit no valido");
    }
    
}
