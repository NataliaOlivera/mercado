package mercado.excepciones;

public class ErrorAlIngresarCuilException extends RuntimeException{

    public ErrorAlIngresarCuilException() {
        super("Se ingreso un cuil no valido");
	}
    
}
