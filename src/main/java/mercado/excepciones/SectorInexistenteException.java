package mercado.excepciones;

public class SectorInexistenteException extends RuntimeException {
    public SectorInexistenteException(){
        super("Sector Inexistente");
    }
}