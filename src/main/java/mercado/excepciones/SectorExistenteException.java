package mercado.excepciones;

public class SectorExistenteException extends RuntimeException {
    public SectorExistenteException(){
        super("Sector Ya Existente");
    }
}