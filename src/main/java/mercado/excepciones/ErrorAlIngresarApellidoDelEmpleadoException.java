package mercado.excepciones;

public class ErrorAlIngresarApellidoDelEmpleadoException extends RuntimeException{

    public ErrorAlIngresarApellidoDelEmpleadoException() {
        super("Apellido ingresado de forma erronea");
    }
    
}
