package mercado.excepciones;

public class ErrorAlIngresarDNIException extends RuntimeException{

    public ErrorAlIngresarDNIException() {
        super("Error al ingresar el DNI del empleado");
    }
    
}
