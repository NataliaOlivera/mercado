package mercado.excepciones;

public class ClienteInexistenteException extends RuntimeException{

    public ClienteInexistenteException() {
        super("Cliente no encontrado");
    }
    
}
