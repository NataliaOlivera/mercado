package mercado.excepciones;

public class PuestoNoDisponibleException extends RuntimeException {
    public PuestoNoDisponibleException(){
        super("Puesto No Disponible");
    }
}