package mercado.excepciones;

public class UsuarioInexistenteException extends RuntimeException{

    public UsuarioInexistenteException() {
        super("El usuario no existe");
    }
    
}
