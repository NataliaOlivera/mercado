package mercado.excepciones;

public class SectorYaDeshabilitadoException extends RuntimeException {
    public SectorYaDeshabilitadoException(){
        super("Sector Ya se encuentra Deshabilitado");
    }
} 
    
