
package mercado.excepciones;

public class FacturasNoEncontradasException extends RuntimeException {
    public FacturasNoEncontradasException(){
        super("No se encontraron facturas");
    }
}

