package mercado.excepciones;

public class LecturaInexistenteException extends RuntimeException {
    public LecturaInexistenteException(){
        super("Lectura No encontrada");
    }
}
