package mercado.excepciones;

public class PuestoEnContratoException extends RuntimeException {
    public PuestoEnContratoException(){
        super("No se puede eliminar puesto, existe un contrato en vigencia asociado ");
    }
}
