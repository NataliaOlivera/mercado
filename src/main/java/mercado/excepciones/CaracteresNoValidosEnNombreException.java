package mercado.excepciones;

public class CaracteresNoValidosEnNombreException extends RuntimeException{

    public CaracteresNoValidosEnNombreException() {
        super("Se ingreso caracteres no validos en el nombre del Responsable del Mercado");
    }
    
}
