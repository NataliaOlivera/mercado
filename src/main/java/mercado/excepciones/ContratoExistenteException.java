package mercado.excepciones;

public class ContratoExistenteException extends RuntimeException{

    public ContratoExistenteException() {
        super("El contrato ya existe");
    }
    
}
