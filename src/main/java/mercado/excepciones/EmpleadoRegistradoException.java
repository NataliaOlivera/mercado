package mercado.excepciones;

public class EmpleadoRegistradoException extends RuntimeException {

    public EmpleadoRegistradoException() {
        super("El empleado ya tiene cuenta activa");
    }
    
}
