package mercado.excepciones;

public class PuestoYaHabilitadoException extends RuntimeException {
    public PuestoYaHabilitadoException(){
        super("Puesto Disponible");
    }
}

