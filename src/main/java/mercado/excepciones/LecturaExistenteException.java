package mercado.excepciones;

public class LecturaExistenteException extends RuntimeException {
    public LecturaExistenteException(){
        super("Lectura Ya Existente");
    }
}