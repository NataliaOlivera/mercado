package mercado.excepciones;

public class CaracteresNoValidosEnApellidoException extends RuntimeException{

    public CaracteresNoValidosEnApellidoException() {
        super("Se ingreso caracteres no validos en el apellido del Responsable del Mercado");
	}
    
}
