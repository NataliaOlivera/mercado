package mercado.excepciones;

public class ErrorAlIngresarNombreDelEmpleadoException extends RuntimeException{

    public ErrorAlIngresarNombreDelEmpleadoException() {
        super("Error al ingresar el nombre del empleado");
    }
    
}
