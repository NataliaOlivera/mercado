package mercado.excepciones;

public class CaracteresNoValidosEnDNIException extends RuntimeException{

    public CaracteresNoValidosEnDNIException() {
        super("Se ingreso caracteres no validos en el dni del Responsable del Mercado");
	}
}
