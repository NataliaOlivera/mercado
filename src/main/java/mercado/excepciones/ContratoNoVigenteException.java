package mercado.excepciones;

public class ContratoNoVigenteException extends RuntimeException {

    public ContratoNoVigenteException() {
        super("Contrato No Vigente");
    }
    
}


