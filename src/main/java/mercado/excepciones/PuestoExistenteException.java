package mercado.excepciones;

public class PuestoExistenteException extends RuntimeException {
    public PuestoExistenteException(){
        super("Puesto Ya Existente");
    }
}