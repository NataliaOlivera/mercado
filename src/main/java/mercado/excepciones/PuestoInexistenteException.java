package mercado.excepciones;

public class PuestoInexistenteException extends RuntimeException {
    public PuestoInexistenteException(){
        super("Puesto Inexistente");
    }
}