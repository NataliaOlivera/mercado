package mercado.excepciones;

public class PuestoYaDeshabilitadoException extends RuntimeException {
    public PuestoYaDeshabilitadoException(){
        super("Puesto Ya Deshabilitado");
    }
}
