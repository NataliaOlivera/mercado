package mercado.excepciones;

public class ContratoInexistenteException extends RuntimeException {
    public ContratoInexistenteException(){
        super("Contrato No encontrado");
    }
}
