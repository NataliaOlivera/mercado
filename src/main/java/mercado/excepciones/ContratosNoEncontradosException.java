package mercado.excepciones;

public class ContratosNoEncontradosException extends RuntimeException {

    public ContratosNoEncontradosException() {
        super("No Existen Contratos en ese periodo de tiempo");
    }
}