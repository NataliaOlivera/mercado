package mercado.excepciones;

public class TelefonoMalIngresadoException extends RuntimeException {

    public TelefonoMalIngresadoException() {
        super("El telefono se ingreso erroneamente");
    }
    
}
