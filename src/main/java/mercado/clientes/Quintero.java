package mercado.clientes;

import java.util.regex.*;

import mercado.excepciones.*;

//hereda de cliente
public class Quintero extends Cliente{

    private String cuil;


//constructor
    public Quintero(Integer numeroDeCliente,String razonSocial,String cuil,String domicilio,String telefono,String email){
      super(numeroDeCliente,razonSocial, domicilio, telefono, email);
      controlarElIngresoDelCuil(cuil);
      this.cuil = cuil;
      this.setBandera(0);
    }

//setter
    public void setCuil(String cuil){
        controlarElIngresoDelCuil(cuil);
       this.cuil=cuil;
    }

//getter
    public String getCuil(){
       return cuil;
     }
    
    @Override
    public String mostrarDatos(){
        return cuil;
    }
    
    ///////////////////////////////CONTROL DE INGRESO DE DATOS////////////////////////////
    
    private static void controlarElIngresoDelCuil(String cuil){
        Pattern pat= Pattern.compile("^([0-9]{11})$");
        Matcher mat=pat.matcher(cuil);
        if(mat.find()==false){
            throw new ErrorAlIngresarCuilException();
        }
    }

}
