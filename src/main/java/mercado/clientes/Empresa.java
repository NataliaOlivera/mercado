package mercado.clientes;

import java.util.regex.*;

import mercado.excepciones.*;

//hereda de cliente
public class Empresa extends Cliente{
    private String cuit;

//constructor
    public Empresa(Integer numeroDeCliente,String razonSocial, String cuit, String domicilio,String telefono,String email){
        super(numeroDeCliente,razonSocial, domicilio, telefono, email);
        controlarElIngresoDelCuit(cuit);
        this.cuit=cuit;
        this.setBandera(1);
    }

   //setter
    public void setCuit(String cuit){
        controlarElIngresoDelCuit(cuit);
        this.cuit=cuit;
    }

//getter
    public String getCuit(){
     return cuit;
    }
    
    @Override
    public String mostrarDatos(){
        return cuit;
    }


    ///////////////////////////////CONTROL DE INGRESO DE DATOS////////////////////////////

    private static void controlarElIngresoDelCuit(String cuit){
        Pattern pat= Pattern.compile("^([0-9]{11})$");
        Matcher mat=pat.matcher(cuit);
        if(mat.find()==false){
            throw new ErrorAlIngresarCuitException();
        }
    }


}