package mercado.clientes;

import java.util.regex.*;

import mercado.excepciones.*;


public abstract class Cliente{
    private Integer numeroDeCliente;
    private String razonSocial;
    private String domicilio;
    private String telefono;
    private String email;
    private Integer bandera;
   
    
    public Cliente(Integer numeroDeCliente,String razonSocial,String domicilio,String telefono,String email){
        controlarIngresoRazonSocial(razonSocial);
        controlarElIngresoDelTelefono(telefono);
        this.razonSocial=razonSocial;
        this.domicilio=domicilio;
        this.telefono=telefono;
        this.email=email;
        this.numeroDeCliente=numeroDeCliente;
    }

    //------GETTERS----------
    public String getRazonSocial() {
        return razonSocial;
    }
    public String getDomicilio() {
        return domicilio;
    }
    public Integer getBandera() {
        return bandera;
    }
    public String getTelefono() {
        return telefono;
    }
    public String getEmail() {
        return email;
    }
    public Integer getNumeroDeCliente() {
        return numeroDeCliente;
    }
   
   

    //--------SETTERS--------
    public void setTelefono(String telefono) {
        controlarElIngresoDelTelefono(telefono);
        this.telefono = telefono;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    public void setRazonSocial(String razonSocial) {
        controlarIngresoRazonSocial(razonSocial);
        this.razonSocial = razonSocial;
    }
    public void setNumeroDeCliente(Integer numeroDeCliente) {
        this.numeroDeCliente=numeroDeCliente;
    }
    public void setBandera(Integer bandera) {
        this.bandera = bandera;
    }
    
    public abstract String mostrarDatos();


    ///////////////////////////////CONTROL DE INGRESO DE DATOS////////////////////////////
    
    private static void controlarIngresoRazonSocial(String razonSocial){
        Pattern pat= Pattern.compile("^([a-zA-z\\s\\s\\s\\s\\s]{0,50})$");
        Matcher mat=pat.matcher(razonSocial);
        if(mat.find()==false){
            throw new ErrorAlIngresarRazonSocialException();
        }
    }

    private static void controlarElIngresoDelTelefono(String telefono){
        Pattern pat= Pattern.compile("^([0-9]{10})$");
        Matcher mat=pat.matcher(telefono);
        if(mat.find()==false){
            throw new TelefonoMalIngresadoException();
        }
    }

   
}
