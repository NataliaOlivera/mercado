package ventana;

import javax.swing.JOptionPane;
import mercado.clientes.Cliente;
import mercado.clientes.Empresa;
import mercado.clientes.Quintero;
import mercado.excepciones.ClienteExistenteException;
import mercado.excepciones.ErrorAlIngresarCuilException;
import mercado.excepciones.ErrorAlIngresarCuitException;
import mercado.excepciones.ErrorAlIngresarRazonSocialException;
import mercado.excepciones.TelefonoMalIngresadoException;
import mercado.mercado.Mercado;


public class AgregarCliente extends javax.swing.JDialog {
   private Mercado mercado;
   
    public AgregarCliente(javax.swing.JDialog parent, boolean modal,Mercado mercado) {
        super(parent, modal);
        this.mercado=mercado;
        initComponents();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        AgregarClientejPanel = new javax.swing.JPanel();
        NumClientejTextField = new javax.swing.JTextField();
        RazonSocialjTextField = new javax.swing.JTextField();
        DomiciliojTextField = new javax.swing.JTextField();
        TelefonojTextField = new javax.swing.JTextField();
        EmailjTextField = new javax.swing.JTextField();
        CuitjTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabelCuit = new javax.swing.JLabel();
        jLabelCuil = new javax.swing.JLabel();
        AgregarClientejButton = new javax.swing.JButton();
        CancelarjButton = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        TipojComboBox1 = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("N� Cliente:");

        jLabel2.setText("Razon Social:");

        jLabel3.setText("Direccion:");

        jLabel4.setText("Telefono:");

        jLabel5.setText("Email:");

        jLabelCuit.setText("Cuit");

        jLabelCuil.setText("Cuil");

        AgregarClientejButton.setText("Agregar");
        AgregarClientejButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarClientejButtonActionPerformed(evt);
            }
        });

        CancelarjButton.setText("Cancelar");
        CancelarjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelarjButtonActionPerformed(evt);
            }
        });

        jLabel8.setText("Tipo:");

        TipojComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Empresa", "Quintero" }));
        TipojComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TipojComboBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout AgregarClientejPanelLayout = new javax.swing.GroupLayout(AgregarClientejPanel);
        AgregarClientejPanel.setLayout(AgregarClientejPanelLayout);
        AgregarClientejPanelLayout.setHorizontalGroup(
            AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AgregarClientejPanelLayout.createSequentialGroup()
                .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AgregarClientejPanelLayout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(AgregarClientejButton, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(84, 84, 84)
                        .addComponent(CancelarjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(AgregarClientejPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabelCuit)
                            .addComponent(jLabelCuil))
                        .addGap(44, 44, 44)
                        .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(AgregarClientejPanelLayout.createSequentialGroup()
                                .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(RazonSocialjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(DomiciliojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(TelefonojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(EmailjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(AgregarClientejPanelLayout.createSequentialGroup()
                                        .addComponent(NumClientejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(37, 37, 37)
                                        .addComponent(jLabel8)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TipojComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(CuitjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        AgregarClientejPanelLayout.setVerticalGroup(
            AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AgregarClientejPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NumClientejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel8)
                    .addComponent(TipojComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RazonSocialjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DomiciliojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(26, 26, 26)
                .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(TelefonojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(EmailjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(29, 29, 29)
                .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AgregarClientejPanelLayout.createSequentialGroup()
                        .addComponent(jLabelCuit)
                        .addGap(4, 4, 4)
                        .addComponent(jLabelCuil))
                    .addComponent(CuitjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AgregarClientejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(AgregarClientejButton, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CancelarjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(AgregarClientejPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(AgregarClientejPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TipojComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TipojComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TipojComboBox1ActionPerformed

    private void CancelarjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelarjButtonActionPerformed
         dispose();
         getParent().setVisible(true);
    }//GEN-LAST:event_CancelarjButtonActionPerformed

    private void AgregarClientejButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarClientejButtonActionPerformed
    try{
              if(this.TipojComboBox1.getSelectedItem()=="Empresa"){
//                    jLabelCuil.setVisible(false);
//                    jLabelCuit.setVisible(true);
                    mercado.agregarClientes(new Empresa(Integer.parseInt(NumClientejTextField.getText()),this.RazonSocialjTextField.getText(),this.CuitjTextField.getText(),this.DomiciliojTextField.getText(),this.TelefonojTextField.getText(),this.EmailjTextField.getText()) {} );
              }
     else{
//                    jLabelCuil.setVisible(true);
//                    jLabelCuit.setVisible(false);
                  mercado.agregarClientes(new Quintero(Integer.parseInt(NumClientejTextField.getText()),this.RazonSocialjTextField.getText(),this.CuitjTextField.getText(),this.DomiciliojTextField.getText(),this.TelefonojTextField.getText(),this.EmailjTextField.getText()) {} );
              
     }
              JOptionPane.showMessageDialog(rootPane, "La operacion se realizo con exito ", "Alerta",1);
              dispose();
              getParent().setVisible(true);
         }catch(ErrorAlIngresarCuilException|ErrorAlIngresarCuitException|TelefonoMalIngresadoException|ErrorAlIngresarRazonSocialException |ClienteExistenteException  u){
                JOptionPane.showMessageDialog(rootPane, u.getMessage(), "Alerta",0);
         }
    }//GEN-LAST:event_AgregarClientejButtonActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AgregarClientejButton;
    private javax.swing.JPanel AgregarClientejPanel;
    private javax.swing.JButton CancelarjButton;
    private javax.swing.JTextField CuitjTextField;
    private javax.swing.JTextField DomiciliojTextField;
    private javax.swing.JTextField EmailjTextField;
    private javax.swing.JTextField NumClientejTextField;
    private javax.swing.JTextField RazonSocialjTextField;
    private javax.swing.JTextField TelefonojTextField;
    private javax.swing.JComboBox<String> TipojComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabelCuil;
    private javax.swing.JLabel jLabelCuit;
    // End of variables declaration//GEN-END:variables

}
